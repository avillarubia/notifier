﻿using Android.App;
using Android.Content;
using Android.Widget;
using Java.Lang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Notifier
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        void Button_Clicked(object sender, EventArgs e)
        {
            int time = Convert.ToInt32(/*timePickerValue*/1000);
            Intent i = new Intent(this, typeof(MyReceiver));

            //PASS CONTEXT,YOUR PRIVATE REQUEST CODE,INTENT OBJECT AND FLAG
            PendingIntent pi = PendingIntent.GetBroadcast(this, 0, i, 0);

            AlarmManager alarmManager = (AlarmManager)GetSystemService(AlarmService);

            alarmManager.Set(AlarmType.RtcWakeup, JavaSystem.CurrentTimeMillis() + (time * 1000), pi);
            Toast.MakeText(this, "Alarm set In: " + time + " seconds", ToastLength.Short).Show();
        }
    }
}
