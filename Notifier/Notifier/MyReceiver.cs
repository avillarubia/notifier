﻿using Android.Content;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Text;

namespace Notifier
{
    [BroadcastReceiver]
    public class MyReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            //Toast for now
            Toast.MakeText(context, "Alarm Ringing!", ToastLength.Short).Show();
        }
    }
}
